import React, { useContext } from 'react';
import '../style/component/SurveyCard.scss';
import Card from 'react-bootstrap/Card';
import trashSolid from '../assets/trash-solid.svg';
import LanguageContext from '../lang/LanguageContext';
import { translate } from '../lang/translate';
import { Form } from 'react-bootstrap';

type Props = {
  id: number;
  type: string;
  question: string;
  required: boolean;
  content?: string[];
  from?: string;
  to?: string;
};

function SurveyCard(props: Props) {
  const { language } = useContext(LanguageContext);
  const surveyViewCardTranslate =
    language == 'fr-FR' ? translate.surveyView.fr : translate.surveyView.en;
  let i = 0;
  return (
    <div className="SurveyCard">
      <Card>
        <Card.Header className="cardHeader">
          <h4>{props.id}</h4>
          <div className="buttons">
            <button className="button">
              <img className="imageButton" src={trashSolid} alt="Delete" />
            </button>
          </div>
        </Card.Header>
        <Card.Body>
          <Card.Text>
            {props.question}
            {props.required && `  (${surveyViewCardTranslate.questionRequired})`}
          </Card.Text>
          {props.type == 'open' && (
            <Form.Group>
              <Form.Text key={i++}>text</Form.Text>
            </Form.Group>
          )}
          {props.type == 'exMultiple' && (
            <Form.Group className="formgroup">
              {props.content?.map((s) => (
                <Form.Check disabled type={'radio'} key={i++} label={s} />
              ))}
            </Form.Group>
          )}
          {props.type == 'inMultiple' && (
            <Form.Group className="formgroup">
              {props.content?.map((s) => (
                <Form.Check disabled type={'checkbox'} key={i++} label={s} />
              ))}
            </Form.Group>
          )}
          {props.type == 'cursor' && (
            <Form.Group className="formgroup">
              <p>{props.from}</p>
              <Form.Range disabled min={0} max={100} step={1} />
              <p>{props.to}</p>
            </Form.Group>
          )}
        </Card.Body>
      </Card>
    </div>
  );
}

export default SurveyCard;
