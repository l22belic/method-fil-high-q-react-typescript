import React, { useContext } from 'react';
import { translate } from '../../lang/translate';
import LanguageContext from '../../lang/LanguageContext';

// A décommenter quand la possibilité de changement sens du QSort sera implémenté
// import RadioComponent from '../RadioComponent';

// A décommenter quand la possibilité de changement sens du QSort sera implémenté
// type Props = {
//   QSortLocation: boolean[];
//   onSelectionSortPosition: (index: number) => void;
// };

function SortManagementView() {
  const { language } = useContext(LanguageContext);
  const sortManagementViewTranslate =
    language == 'fr-FR' ? translate.sortManagementView.fr : translate.sortManagementView.en;

  return (
    <div className="Qsort_config">
      <div>
        {/* Information */}
        <h2>{sortManagementViewTranslate.QSortHelp}</h2>
        <p>{sortManagementViewTranslate.QSortHelpTextIntro}</p>

        <h5>{sortManagementViewTranslate.QSortHelpAdd}</h5>
        <p>{sortManagementViewTranslate.QSortHelpTextAdd}</p>

        <h5>{sortManagementViewTranslate.QSortHelpDelete}</h5>
        <p>{sortManagementViewTranslate.QSortHelpTextDelete}</p>

        {/* A décommenter quand la possibilité de changement sens du QSort sera implémenté */}
        {/* Sort location */}
        {/* <div>
          <h5>{sortManagementViewTranslate.QSortPosition}</h5>
          <RadioComponent
            label={sortManagementViewTranslate.top}
            radioGroup="location"
            selected={props.QSortLocation[0]}
            onClick={() => {
              props.onSelectionSortPosition(0);
            }}
          ></RadioComponent>
          <RadioComponent
            label={sortManagementViewTranslate.bottom}
            radioGroup="location"
            selected={props.QSortLocation[1]}
            onClick={() => {
              props.onSelectionSortPosition(1);
            }}
          ></RadioComponent>
        </div> */}
      </div>
    </div>
  );
}

export default SortManagementView;
