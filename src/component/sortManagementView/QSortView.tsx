import '../../style/component/sortManagementView/QSortView.scss';
import ButtonComponent from '../ButtonComponent';

type Props = {
  qsortMatrice: number[][];
  qsortColName: string[];
  sendQsortMatrice: (matrice: number[][], colName: string[]) => void;
};

function QSortView(props: Props) {
  function addBlock(line: number, col: number) {
    const newMatrice: number[][] = [...props.qsortMatrice];
    const colName: string[] = [...props.qsortColName];
    // Extémité
    if (line == 0 && col == newMatrice[0].length - 1) {
      for (let index = 0; index < newMatrice.length; index++) {
        newMatrice[index].push(0);
        newMatrice[index].unshift(0);
      }

      newMatrice[line][col + 1] = 1;
      newMatrice[line][0] = -1;

      colName.push('');
      colName.unshift('');
    } else if (line == newMatrice.length - 1) {
      // Si ajout dernière ligne
      if (col == newMatrice[line].length / 2 - 1) {
        newMatrice[line][col] = 2;
      } else {
        newMatrice[line][col] = 1;
        newMatrice[line][newMatrice[line].length - col - 2] = -1;
      }
      newMatrice.push(Array(newMatrice[line].length).fill(0));
    } else if (col == newMatrice[line].length / 2 - 1) {
      // Si millieu
      newMatrice[line][col] = 2;
    } else {
      // Pas extrimité
      // Ajout droite
      newMatrice[line][col] = 1;
      // Ajout gauche
      newMatrice[line][newMatrice[line].length - col - 2] = -1;
    }
    props.sendQsortMatrice(newMatrice, colName);
  }

  function deleteBlock(line: number, col: number) {
    const newMatrice: number[][] = [...props.qsortMatrice];
    const colName: string[] = [...props.qsortColName];
    // Si un block
    if (
      ((newMatrice.length >= 2 && newMatrice[0].length > 2) ||
        (newMatrice.length > 2 && newMatrice[0].length >= 2)) &&
      (newMatrice[line][col] == 1 || newMatrice[line][col] == 2)
    ) {
      // Le millieu
      if (newMatrice[line][col] == 2) {
        //Si c'est le dernier block du milieu
        if (newMatrice[line + 1][col] == 0 && line != 0) {
          newMatrice[line][col] = 0;
        }
      } else {
        // Pas un block du milieu

        // Si un block droit
        if (newMatrice[line][col] == 1) {
          //  et dernier de sa colonne
          if (line != 0 && newMatrice[line + 1][col] == 0) {
            newMatrice[line][col] = 0;
            newMatrice[line][newMatrice[line].length - col - 2] = 0;
          }

          // ou dernier de la premier ligne
          if (line == 0 && newMatrice[line][col + 1] != 1 && newMatrice[line + 1][col] != 1) {
            newMatrice[line][col] = 0;
            newMatrice[line][newMatrice[line].length - col - 2] = 0;

            colName.pop();
            colName.shift();
          }
        }
      }

      // Supprime la dernier ligne de la matrice si les deux dernière ligne ne contient que des zéro
      if (
        newMatrice.length > 2 &&
        !newMatrice[newMatrice.length - 2].includes(1) &&
        !newMatrice[newMatrice.length - 2].includes(2)
      ) {
        newMatrice.pop();
      }

      // Supprime les colonnes qui ne contient que des 0
      let removeRight = true;
      for (let index = 0; index < newMatrice.length; index++) {
        if (
          newMatrice[index][newMatrice[0].length - 2] == 1 ||
          newMatrice[index][newMatrice[0].length - 2] == 2
        ) {
          removeRight = false;
        }
      }

      if (removeRight == true) {
        for (let index = 0; index < newMatrice.length; index++) {
          newMatrice[index].pop();
          newMatrice[index].shift();
        }
      }

      props.sendQsortMatrice(newMatrice, colName);
    }
  }

  function updateColName(col: number, value: string) {
    const colName = [...props.qsortColName];
    colName[col] = value;
    props.sendQsortMatrice(props.qsortMatrice, colName);
  }

  return (
    <>
      <h1 className="center">QSort</h1>
      <div className="qsort_view">
        {props.qsortMatrice.map((el, line) => (
          <div key={'line' + line} className="qsort_line">
            {el.map((el2, col) =>
              el2 != 0 ? (
                // Cas case a voir
                el2 == 2 ? (
                  <div className="qsort_block_with_text" key={'col' + line + col}>
                    {line == 0 ? (
                      <input
                        type="text"
                        defaultValue={props.qsortColName[col]}
                        name=""
                        id=""
                        onChange={(e) => updateColName(col, e.target.value)}
                      />
                    ) : (
                      ''
                    )}
                    <span
                      className={
                        props.qsortMatrice[line + 1][col] == 0 && line != 0
                          ? 'delete_block qsort_block_midle'
                          : 'qsort_block_midle'
                      }
                      onClick={() => deleteBlock(line, col)}
                    ></span>
                  </div>
                ) : (
                  <div className="qsort_block_with_text" key={'col' + line + col}>
                    {line == 0 ? (
                      <input
                        type="text"
                        defaultValue={props.qsortColName[col]}
                        name=""
                        id=""
                        onChange={(e) => updateColName(col, e.target.value)}
                      />
                    ) : (
                      ''
                    )}
                    <span
                      className={
                        el2 == 1 &&
                        ((line != 0 && props.qsortMatrice[line + 1][col] == 0) ||
                          (line == 0 &&
                            props.qsortMatrice[line][col + 1] != 1 &&
                            props.qsortMatrice[line + 1][col] != 1))
                          ? 'delete_block qsort_block_visible'
                          : 'qsort_block_visible'
                      }
                      key={'col' + line + col}
                      onClick={() => deleteBlock(line, col)}
                    ></span>
                  </div>
                )
              ) : line == 0 && col == props.qsortMatrice[line].length - 1 ? (
                // Cas si case tout a droite
                <ButtonComponent
                  key={'col' + line + col}
                  buttonText="+"
                  isSecondary={true}
                  className="qsort_block_add"
                  onClick={() => {
                    addBlock(line, col);
                  }}
                ></ButtonComponent>
              ) : props.qsortMatrice[line - 1][col] != -1 &&
                props.qsortMatrice[line - 1][col] != 0 ? (
                // Cas si case en haut d'une colonne
                <ButtonComponent
                  key={'col' + line + col}
                  buttonText="+"
                  isSecondary={true}
                  className="qsort_block_add"
                  onClick={() => {
                    addBlock(line, col);
                  }}
                ></ButtonComponent>
              ) : (
                // Cas case vide
                <span className="qsort_block_invisible" key={'col' + line + col}></span>
              )
            )}
          </div>
        ))}
      </div>
    </>
  );
}

export default QSortView;
