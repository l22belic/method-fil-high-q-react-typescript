import '../../style/component/sortManagementView/Tas3View.scss';
import { Container } from 'react-bootstrap';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import rectangle from '../../assets/Rectangle.png';

type Props = {
  SortElmt1: string;
  SortElmt2: string;
  SortElmt3: string;
};

function Tas3View(props: Props) {
  return (
    <Container className="space">
      <h1 className="center">3-Tas</h1>
      <Row className="columns">
        <Col className="space">
          <div className="center">
            <h5>{props.SortElmt1}</h5>
          </div>
          <Card className="bg-dark text-white">
            <Card.Img src={rectangle} alt="tas" />
          </Card>
        </Col>
        <Col className="space">
          <div className="center">
            <h5>{props.SortElmt2}</h5>
          </div>
          <Card className="bg-dark text-white">
            <Card.Img src={rectangle} alt="tas" />
          </Card>
        </Col>
        <Col className="space">
          <div className="center">
            <h5>{props.SortElmt3}</h5>
          </div>
          <Card className="bg-dark text-white">
            <Card.Img src={rectangle} alt="tas" />
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

export default Tas3View;
