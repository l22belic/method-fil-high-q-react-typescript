import React, { useContext, useState } from 'react';
import CheckBoxComponent from '../CheckBoxComponent';
import ButtonComponent from '../ButtonComponent';
import Form from 'react-bootstrap/esm/Form';
import ListGroup from 'react-bootstrap/esm/ListGroup';
import LanguageContext from '../../lang/LanguageContext';
import { translate } from '../../lang/translate';
import '../../style/component/sortManagementView/Tas3Section.scss';

type Props = {
  sendData: (
    activated3Tas: boolean,
    activatedSuccessiveDrawing: boolean,
    tas3CustomName: string[][],
    selecTed3TasCustomName?: string[]
  ) => void;
  activated3Tas: boolean;
  activatedSuccessiveDrawing: boolean;
  tas3CustomName: string[][];
};

function Tas3Section(props: Props) {
  const { language } = useContext(LanguageContext);
  const [activated3Tas, setactivated3Tas] = useState<boolean>(props.activated3Tas);
  const [activatedSuccessiveDrawing, setActivatedSuccessiveDrawing] = useState<boolean>(
    props.activatedSuccessiveDrawing
  );
  const [tas3CustomName, setTas3CustomName] = useState<string[][]>(props.tas3CustomName);
  const addCustome3TasName: string[] = ['', '', ''];

  const sortManagementViewTranslate =
    language == 'fr-FR' ? translate.sortManagementView.fr : translate.sortManagementView.en;
  function addCustome3Tas(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault(); // 👈️ prevent page refresh
    const nextTas3CustomeName = [
      addCustome3TasName,
      ...tas3CustomName.slice(0, tas3CustomName.length + 1)
    ];
    setTas3CustomName(nextTas3CustomeName);
    props.sendData(activated3Tas, activatedSuccessiveDrawing, nextTas3CustomeName, undefined);
    event.currentTarget.reset();
  }

  return (
    <div className="tas_config">
      {/* CheckBox activate config 3-Tas */}
      <CheckBoxComponent
        label={sortManagementViewTranslate.activated3Tas}
        selected={activated3Tas}
        onClick={() => {
          props.sendData(!activated3Tas, activatedSuccessiveDrawing, tas3CustomName, undefined);
          if (!activated3Tas == false) {
            document.getElementById('tas_config_contain')?.classList.add('disabled');
          } else {
            document.getElementById('tas_config_contain')?.classList.remove('disabled');
          }
          setactivated3Tas(!activated3Tas);
        }}
      ></CheckBoxComponent>

      <div id="tas_config_contain" className={activated3Tas ? '' : 'disabled'}>
        {/* Activate  successive drawing*/}
        <CheckBoxComponent
          label={sortManagementViewTranslate.activatedSuccessiveDrawing}
          selected={activatedSuccessiveDrawing}
          onClick={() => {
            setActivatedSuccessiveDrawing(!activatedSuccessiveDrawing);
            props.sendData(activated3Tas, !activatedSuccessiveDrawing, tas3CustomName, undefined);
          }}
        ></CheckBoxComponent>
        {/* Custom 3-Tas name */}
        <div>
          <h5>{sortManagementViewTranslate.Tas3}</h5>
          <ListGroup className="custom_select_3tas">
            {tas3CustomName.map((el, i) => (
              <ListGroup.Item
                action
                variant="primary"
                key={el[0] + i}
                onClick={() => {
                  props.sendData(activated3Tas, activatedSuccessiveDrawing, tas3CustomName, el);
                }}
              >
                <span>{el[0]}</span>
                <span>{el[1]}</span>
                <span>{el[2]}</span>
              </ListGroup.Item>
            ))}
          </ListGroup>
        </div>

        {/* Add Custome 3-Tas name */}
        <Form onSubmit={addCustome3Tas} className="add_custome_3tas">
          <h5>{sortManagementViewTranslate.custome}</h5>
          <ul>
            <li>
              <Form.Control
                required
                type="text"
                placeholder="Pour Désaccord"
                onChange={(e) => (addCustome3TasName[0] = e.target.value)}
              />
            </li>
            <li>
              <Form.Control
                required
                type="text"
                placeholder="Pour Neutre"
                onChange={(e) => (addCustome3TasName[1] = e.target.value)}
              />
            </li>
            <li>
              <Form.Control
                required
                type="text"
                placeholder="Pour D'accord"
                onChange={(e) => (addCustome3TasName[2] = e.target.value)}
              />
            </li>
          </ul>
          <ButtonComponent
            buttonText="Ajouter"
            onClick={() => {
              //
            }}
            type="submit"
            className="button_add"
          ></ButtonComponent>
        </Form>
      </div>
    </div>
  );
}

export default Tas3Section;
