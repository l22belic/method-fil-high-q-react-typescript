import React from 'react';
import '../style/component/RadioGroupComponent.scss';

type Props = {
  radioGroup?: string;
  label: string;
  selected: boolean;
  onClick: () => void;
};

RadioComponent.defaultProps = {
  selected: false
};

function RadioComponent(props: Props) {
  return (
    <div className="container_radio_group_component">
      <input
        type="radio"
        name={props.radioGroup}
        id={props.label}
        checked={props.selected}
        onChange={props.onClick}
      />
      <label htmlFor={props.label}>{props.label}</label>
    </div>
  );
}

export default RadioComponent;
