import React from 'react';
import '../style/component/ButtonComponent.scss';
import Button from 'react-bootstrap/esm/Button';

type TypeButton = 'button' | 'submit' | 'reset' | undefined;

type Props = {
  buttonText: string;
  isSecondary?: boolean;
  className?: string;
  disabled?: boolean;
  type: TypeButton;
  onClick: () => void;
};

ButtonComponent.defaultProps = {
  isSecondary: false,
  type: 'button'
};

function ButtonComponent(props: Props) {
  return (
    <>
      {props.isSecondary == false ? (
        <Button
          type={props.type}
          className={'button ' + props.className + (props.disabled ? ' disabled' : '')}
          variant="primary"
          onClick={props.onClick}
        >
          {props.buttonText}
        </Button>
      ) : (
        <Button
          type={props.type}
          className={'button_secondary ' + props.className + (props.disabled ? ' disabled' : '')}
          variant="outline_primary"
          onClick={props.onClick}
        >
          {props.buttonText}
        </Button>
      )}
    </>
  );
}

export default ButtonComponent;
