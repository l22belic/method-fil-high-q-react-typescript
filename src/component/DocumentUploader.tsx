import React, { useContext, useState } from 'react';
import '../style/component/DocumentUploader.scss';
import LanguageContext from '../lang/LanguageContext';
import { translate } from '../lang/translate';
import deleteIcon from '../assets/delete_icon.svg';
import uploadIcon from '../assets/upload_icon.svg';

type DocumentUploaderProps = {
  id: number;
  onDocumentUpload: (url: string) => void;
  onRemoveDocument: () => void;
  isAudio?: boolean;
  isImage?: boolean;
  file?: string;
};

function DocumentUploader(props: DocumentUploaderProps) {
  const { language } = useContext(LanguageContext);

  const documentUploaderTranslate =
    language == 'fr-FR' ? translate.documentUploader.fr : translate.documentUploader.en;

  const [dragging, setDragging] = useState(false);
  const [file, setFile] = useState('');

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setDragging(true);
  };

  const handleDragLeave = () => {
    setDragging(false);
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setDragging(false);
    const file = event.dataTransfer.files[0];
    props.onDocumentUpload(URL.createObjectURL(file));
    setFile(URL.createObjectURL(file));
  };

  const handleFileInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files && event.target.files[0];
    if (file) {
      props.onDocumentUpload(URL.createObjectURL(file));
      setFile(URL.createObjectURL(file));
    }
  };

  return (
    <div>
      {props.isImage && file && (
        <div className="previsualisation">
          <img src={file} className="imageVisualisation" />
          <img
            src={deleteIcon}
            className="cross"
            onClick={() => {
              props.onRemoveDocument();
              setFile('');
            }}
          />
        </div>
      )}
      {props.isAudio && file && (
        <div className="previsualisation">
          <audio src={file} controls className="imageVisualisation">
            Cool
          </audio>
          <img
            src={deleteIcon}
            className="cross"
            onClick={() => {
              props.onRemoveDocument();
              setFile('');
            }}
          />
        </div>
      )}
      {file == '' && (
        <div>
          <label htmlFor={'files-' + props.id} className="uploadSection">
            <div
              className={`uploader ${dragging ? 'dragging' : ''}`}
              onDragOver={handleDragOver}
              onDragLeave={handleDragLeave}
              onDrop={handleDrop}
            >
              {dragging ? (
                <div className="dropIndicator">
                  {props.isAudio && !props.isImage
                    ? documentUploaderTranslate.dropAudioLabel
                    : documentUploaderTranslate.dropImageLabel}
                </div>
              ) : (
                <div className="uploadPrompt">
                  <p>
                    {props.isAudio && !props.isImage
                      ? documentUploaderTranslate.dragDropAudioHere
                      : documentUploaderTranslate.dragDropImageHere}
                  </p>
                </div>
              )}
              <img src={uploadIcon} width={50} />
            </div>
          </label>
          <input
            className="documentInput"
            type="file"
            accept={props.isAudio ? 'audio/*' : 'image/*'}
            onChange={handleFileInputChange}
            id={'files-' + props.id}
            hidden
          />
        </div>
      )}
    </div>
  );
}

export default DocumentUploader;
