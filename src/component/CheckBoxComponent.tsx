import React from 'react';
import '../style/component/CheckBoxGroupComponent.scss';

type Props = {
  checkboxGroup?: string;
  label: string;
  selected: boolean;
  onClick: () => void;
};

function CheckBoxComponent(props: Props) {
  return (
    <div className="container_checkbox_group_component">
      <input
        type="checkbox"
        name={props.checkboxGroup}
        id={props.label}
        checked={props.selected}
        onChange={props.onClick}
      />
      <label htmlFor={props.label}>{props.label}</label>
    </div>
  );
}

export default CheckBoxComponent;
