import React from 'react';
import '../style/component/StatementCard.scss';
import Card from 'react-bootstrap/Card';

import trashSolid from '../assets/trash-solid.svg';
import { Statement } from '../view/DataContext';

type Props = {
  id: number;
  cardText?: string;
  cardImage?: string;
  cardAudio?: string;
  onDelete: () => void;
  onEdit: (st: Statement) => void;
};

StatementCard.defaultProps = {
  isSecondary: 'false',
  onDelete: () => {
    console.log('delete action clicked');
  },
  onEdit: (st: Statement) => {
    console.log('edit action clicked' + st);
  }
};

function StatementCard(props: Props) {
  return (
    <div className="StatementCard">
      <Card>
        <Card.Header className="cardHeader">
          <h4>{props.id}</h4>
          <div className="buttons">
            <button className="button" onClick={() => props.onDelete()}>
              <img className="imageButton" src={trashSolid} alt="Delete" />
            </button>
            {/* <button
              className="button"
              onClick={() => {
                const st: Statement = {
                  id: props.id,
                  audio: props.cardAudio,
                  description: props.cardText,
                  image: props.cardImage
                };
                props.onEdit(st);
              }}
            >
              <img className="imageButton" src={penSolid} alt="Modify" />
            </button> */}
          </div>
        </Card.Header>
        <Card.Body>{props.cardText && <Card.Text>{props.cardText}</Card.Text>}</Card.Body>
        {props.cardImage && (
          <Card.Img className="StatementImage" variant="bottom" src={props.cardImage} />
        )}
        {props.cardAudio && (
          <Card.Footer>
            <audio controls src={props.cardAudio} />
          </Card.Footer>
        )}
      </Card>
    </div>
  );
}

export default StatementCard;
