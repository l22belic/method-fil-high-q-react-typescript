import React from 'react';
import '../style/component/ButtonGroupComponent.scss';
type Props = {
  buttonGroup?: string;
  buttonText: string;
  selected: boolean;
  onClick: () => void;
};

ButtonGroupComponent.defaultProps = {
  selected: false
};

function ButtonGroupComponent(props: Props) {
  return (
    <div className="container_button_group_component">
      <input
        type="radio"
        name={props.buttonGroup}
        id={props.buttonText}
        checked={props.selected}
        onChange={props.onClick}
      />
      <label htmlFor={props.buttonText}>{props.buttonText}</label>
    </div>
  );
}

export default ButtonGroupComponent;
