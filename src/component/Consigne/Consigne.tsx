import React from 'react';
import '../../style/component/ButtonComponent.scss';
import '../../style/component/Consigne/Consigne.scss';
import { Form } from 'react-bootstrap';

type Props = {
  name: string;
  placeholder: string;
};

Consigne.defaultProps = {
  name: 'Default',
  placehodler: 'Placeholder'
};

function Consigne(props: Props) {
  return (
    <div className="mainDivConsigne">
      <p className="textForm">{props.name}</p>
      <Form.Control placeholder={props.placeholder}></Form.Control>
    </div>
  );
}

export default Consigne;
