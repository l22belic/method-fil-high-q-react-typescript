export const translate = {
  documentUploader: {
    en: {
      dropImageLabel: 'Drop image here',
      dropAudioLabel: 'Drop audio here',
      dragDropImageHere: 'Drop image here',
      dragDropAudioHere: 'Drop audio here'
    },
    fr: {
      dropImageLabel: "Déposer l'image ici",
      dropAudioLabel: 'Déposer la piste audio ici ou',
      dragDropImageHere: "Déposer l'image ici",
      dragDropAudioHere: "Déposer l'audio ici"
    }
  },
  statementView: {
    en: {
      creationTitle: 'Creation',
      statementTitle: 'Your statements',
      uploadImage: 'Upload your image',
      uploadAudio: 'Upload your audio',
      createButtonLabel: 'Add',
      importFileButtonLabel: 'Import a file (.sta, .txt or .csv)',
      downloadStaButtonLabel: 'Download the file in STA format',
      audioLabel: 'Audio (optionnal)',
      imageLabel: 'Picture (optionnal)',
      descriptionLabel: 'Description (optionnal)',
      textAreaPlaceholder: 'Enter the description'
    },
    fr: {
      creationTitle: 'Création',
      statementTitle: 'Vos déclarations',
      uploadImage: 'Déposer votre image',
      uploadAudio: 'Déposer votre audio',
      createButtonLabel: 'Créer',
      importFileButtonLabel: 'Importer un fichier (.sta, .txt ou .csv)',
      downloadStaButtonLabel: 'Télécharger le fichier au format STA',
      audioLabel: 'Audio (facultatif)',
      imageLabel: 'Image (facultatif)',
      descriptionLabel: 'Description (facultatif)',
      textAreaPlaceholder: 'Entrer la description'
    }
  },
  sortManagementView: {
    en: {
      QSortHelp: 'QSort helper',
      QSortHelpTextIntro: `You are in the configuration section of the QSort. We'll explain how it all works. In order to learn how the Qsort works on the right`,
      QSortHelpAdd: `Add a block in the QSort`,
      QSortHelpTextAdd: `To add a block in the QSort you must click on the button with a "+"`,
      QSortHelpDelete: `Deleting a block in the QSort`,
      QSortHelpTextDelete: `To delete a block in the QSort, click on the blocks with a "trash can icon"`,
      QSortPosition: 'QSort Positionnement',
      top: 'Top',
      bottom: 'Bottom',
      activated3Tas: 'Activate 3-Tas',
      custome: 'Customise',
      activatedSuccessiveDrawing: 'Statement by successive drawing',
      Tas3: 'Definition of 3-Heap'
    },
    fr: {
      QSortHelp: 'Aide pour le QSort',
      QSortHelpTextIntro: `Vous vous trouvez dans la section configuration du QSort. Nous allons vous expliquez comment tout cela fonctionne. Afin d'apprendre comment fonctionne le Qsort à droite`,
      QSortHelpAdd: `Ajouter un blocs dans le QSort`,
      QSortHelpTextAdd: `Pour ajouter un blocs dans le QSort il faut cliquer sur le bouton avec un "+"`,
      QSortHelpDelete: `Suppressions d'un blocs dans le QSort`,
      QSortHelpTextDelete: `Pour supprimer un blocs dans le QSort, cliquer sur les blocs avec une "icone de poubelle"`,
      QSortPosition: 'Qsort positionnement des valeurs',
      top: 'Haut',
      bottom: 'Bas',
      activated3Tas: 'Activer le 3-Tas',
      custome: 'Personnalisé',
      activatedSuccessiveDrawing: 'Statement par tirage successif',
      Tas3: 'Définition du 3-Tas'
    }
  },
  surveyView: {
    en: {
      questionType_open: 'Open question',
      questionType_exMultiple: 'Exclusive multiple choice',
      questionType_inMultiple: 'Inclusive multiple choice',
      questionType_cursor: 'Cursor',
      questionPlaceHolder: 'Your question ...',
      questionRequired: 'Answer required',
      questionType_cursorFrom: 'From',
      questionType_cursorTo: 'To',
      createButtonLabel: 'Add'
    },
    fr: {
      questionType_open: 'Question ouverte',
      questionType_exMultiple: 'Choix multiple exclusif',
      questionType_inMultiple: 'Choix multiple inclusif',
      questionType_cursor: 'Curseur',
      questionPlaceHolder: 'Votre question ...',
      questionRequired: 'Réponse obligatoire',
      questionType_cursorFrom: 'Depart',
      questionType_cursorTo: 'Arrivée',
      createButtonLabel: 'Créer'
    }
  },
  saveView: {
    en: {
      GenerateButton: 'Generate',
      GenerateText:
        'Save your study settings! This will download a JSON file file containing your study.\n' +
        ' For now, if you wish to publish your study, please contact aurelien.milliat@imt-atlantique.fr',
      SaveButton: 'Save',
      SaveText:
        'Save your study, and it will be associated with your account in the list of your studies later to modify it or just consult it.\n' +
        ' (This not yet developed)'
    },
    fr: {
      GenerateButton: 'Générer',
      GenerateText:
        'Enregistrer votre étude paramétré ! Cela vous téléchargera un fichier de type JSON contenant votre étude.\n' +
        ' Pour le momenent si vous souhaitez publié votre étude merci de contacter aurelien.milliat@imt-atlantique.fr',
      SaveButton: 'Savegarder',
      SaveText:
        'Sauvegarder votre étude, elle sera associé à votre compte et vous pourrez la retrouver dans la liste de vos études plus tard pour la modifier ou juste la consulter.\n' +
        ' (Cette option est pas encore développé)'
    }
  },
  rulesView: {
    fr: {
      placeholderInput: 'Entrez un texte',
      introGenInput: 'Introduction générale',
      zoomInput: 'Texte de zoom',
      startInput: 'Texte de bouton début',
      nextInput: 'Texte de bouton suivant',
      mailInput: 'Texte envoie de mail',
      sansSaveInput: 'Texte bouton de fin sans sauvegarde',
      finTestInput: 'Texte bouton de fin de test et sauvegarde',
      btnInput: 'Valider',
      statementInput: 'Présentation des statements',
      tasInput: 'Introduction à la phase 3tas',
      QSortInput: 'Introduction au QSort',
      endText: 'Texte de fin',
      agreeInput: 'Texte Agree',
      disagreeInput: 'Texte Disagree',
      dynamiqueInput: 'Sur les statements dynamique',
      warningInput: 'Avertissement'
    },
    en: {
      placeholderInput: 'Enter a comment',
      introGenInput: 'General introduction',
      zoomInput: 'Zoom text',
      startInput: 'Start button text',
      nextInput: 'Next button text',
      mailInput: 'Send mail text',
      sansSaveInput: 'End button text without save',
      finTestInput: 'End button text with save',
      btnInput: 'Validate',
      statementInput: 'Statement presentation',
      tasInput: '3tas presentation',
      QSortInput: 'QSort presentation',
      endText: 'End text',
      agreeInput: 'Agree text',
      disagreeInput: 'Disagree text',
      dynamiqueInput: 'Dynamic statements',
      warningInput: 'Warning'
    }
  },
  loginView: {},
  tabView: {
    en: {
      statementTitle: 'Statement',
      sortTitle: 'Sort',
      surveyTitle: 'Survey',
      rulesTitle: 'Rules',
      saveTitle: 'Save'
    },
    fr: {
      statementTitle: 'Statement',
      sortTitle: 'Gestion du tri',
      surveyTitle: 'Questionnaire',
      rulesTitle: 'Consigne',
      saveTitle: 'Sauvegarder'
    }
  }
};
