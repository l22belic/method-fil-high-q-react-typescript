import React from 'react';

type LanguageContextType = {
  language: string;
};

const LanguageContext = React.createContext<LanguageContextType>({
  language: 'fr'
});

export default LanguageContext;
