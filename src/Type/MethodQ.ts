type QSort = {
  [key: string]: {
    limit: number;
    title: string;
  };
};

type Statement = {
  id: number;
  type: string;
  value: string;
  description?: string;
  illustration?: string;
};

type ThreeState = {
  type: string;
  title: string;
};

type Parameter = {
  name: string;
  value: string;
};

type ExoQuizz = {
  id: string;
  type: string;
  text: string;
  values?: string[];
  parameters?: Parameter[];
};

export type MethodQ = {
  serverUrlSubmit: string;
  opacityAnimationInterval: number;
  opacityAnimationDuration: number;
  scalingAnimationInterval: number;
  scalingAnimationDuration: number;
  scalingAnimationMaxValue: number;
  scalingAnimationMinValue: number;
  dragAndDropClone: boolean;
  qSortConfiguration: {
    qSort: QSort;
    statementsAlignement: string;
    isCrescent: string;
  };
  statements: Statement[];
  showStatementsFirst: boolean;
  isTraceActive: boolean;
  isThreeStatesDeck: boolean;
  threeStates: ThreeState[];
  startButton: string;
  continueButton: string;
  sendButton: string;
  notSendButton: string;
  zoomTitle: string;
  errorText: string;
  introText: string;
  presentationText: string;
  threeStatesText: string;
  qSortText: string;
  quizzText: string;
  finishText: string;
  agreePostText: string;
  disagreePostText: string;
  dynamicPostText: string;
  requiredText: string;
  exoQuizzNotFull: string;
  isInterviewActive: string;
  dynamicType: number;
  dynamicStatementNumber: number;
  isExogenActive: boolean;
  isSendButtonsAtExogen: boolean;
  exoQuizz: ExoQuizz[];
};
