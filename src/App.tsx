import './App.scss';
import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import './style/test.scss';
import LanguageContext from './lang/LanguageContext';
import TabView from './view/TabView';
import LoginView from './view/LoginView';

function App() {
  const [language] = useState<string>(navigator.language);
  const [vue, setVue] = useState(false);
  return (
    <>
      <LanguageContext.Provider value={{ language }}>
        {!vue ? <LoginView onClick={() => setVue(true)} /> : <TabView />}
      </LanguageContext.Provider>
    </>
  );
}

export default App;
