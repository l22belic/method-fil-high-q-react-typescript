import React, { useState } from 'react';
import '../style/SortManagementView.scss';
import ButtonGroupComponent from '../component/ButtonGroupComponent';
import QSortSection from '../component/sortManagementView/QSortSection';
import Tas3Section from '../component/sortManagementView/Tas3Section';
import Tas3View from '../component/sortManagementView/Tas3View';
import QSortView from '../component/sortManagementView/QSortView';

function SortManagementView() {
  const [sortGroupSelected, setSortGroupSelected] = useState<boolean[]>([true, false]);

  // A décommenter quand la possibilité de changement sens du QSort sera implémenté
  // const [QSortLocation, setQSortLocation] = useState<boolean[]>([true, false]);

  const [selected3Tas, setSelected3Tas] = useState<string[]>(['Désaccord', 'Neutre', "D'accord"]);
  const [isActivatedSuccessiveDrawing, setIsActivatedSuccessiveDrawing] = useState<boolean>(false);
  const [isActivated3Tas, setIsActivated3Tas] = useState<boolean>(true);
  const [tas3CustomName, setTas3CustomName] = useState<string[][]>([
    ['Désaccord', 'Neutre', "D'accord"],
    ['Pas ok', 'JSP', 'OK']
  ]);
  const [qsortMatrice, setQsortMatrice] = useState<number[][]>([
    [-1, -1, 2, 1, 1, 0],
    [0, -1, 0, 1, 0, 0],
    [0, 0, 0, 0, 0, 0]
  ]);
  const [qsortColName, setQsortColName] = useState<string[]>(['-2', '-1', '0', '1', '2', '']);

  // A décommenter quand la possibilité de changement sens du QSort sera implémenté
  // function selectionSortPosition(index: number) {
  //   index == 0 ? setQSortLocation([true, false]) : setQSortLocation([false, true]);
  // }

  function getTas3SectionData(
    activated3Tas: boolean,
    activatedSuccessiveDrawing: boolean,
    newTas3CustomName: string[][],
    selectedTas3CustomName?: string[]
  ) {
    setIsActivated3Tas(activated3Tas);
    setIsActivatedSuccessiveDrawing(activatedSuccessiveDrawing);
    setTas3CustomName(newTas3CustomName);
    setSelected3Tas(selectedTas3CustomName ? selectedTas3CustomName : selected3Tas);
  }

  function getQsortMatrice(matrice: number[][], colName: string[]) {
    setQsortMatrice(matrice);
    setQsortColName(colName);
  }

  return (
    <div className="container_sort_management_view">
      <div>
        {/* Select sort */}
        <div>
          <ButtonGroupComponent
            buttonGroup={'sort'}
            selected={sortGroupSelected[0]}
            buttonText={'QSort'}
            onClick={() => {
              setSortGroupSelected([true, false]);
            }}
          ></ButtonGroupComponent>
          <ButtonGroupComponent
            buttonGroup={'sort'}
            buttonText={'3-Tas'}
            selected={sortGroupSelected[1]}
            onClick={() => {
              setSortGroupSelected([false, true]);
            }}
          ></ButtonGroupComponent>
        </div>
        {sortGroupSelected[0] ? (
          <QSortSection
          // A décommenter quand la possibilité de changement sens du QSort sera implémenté
          // QSortLocation={QSortLocation}
          // onSelectionSortPosition={selectionSortPosition}
          ></QSortSection>
        ) : (
          <Tas3Section
            sendData={getTas3SectionData}
            activated3Tas={isActivated3Tas}
            activatedSuccessiveDrawing={isActivatedSuccessiveDrawing}
            tas3CustomName={tas3CustomName}
          ></Tas3Section>
        )}
      </div>
      <div>
        {sortGroupSelected[0] ? (
          <QSortView
            qsortColName={qsortColName}
            qsortMatrice={qsortMatrice}
            sendQsortMatrice={getQsortMatrice}
          ></QSortView>
        ) : (
          <>
            <Tas3View
              SortElmt1={selected3Tas[0]}
              SortElmt2={selected3Tas[1]}
              SortElmt3={selected3Tas[2]}
            ></Tas3View>
          </>
        )}
      </div>
    </div>
  );
}

export default SortManagementView;
