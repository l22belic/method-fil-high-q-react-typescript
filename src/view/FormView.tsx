import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload, faPlus } from '@fortawesome/free-solid-svg-icons';
import '../style/LoginView.scss';
type Props = {
  onClick: () => void;
};
function FormView(props: Props) {
  return (
    <div className="form flexColumn">
      <div className="largeDiv flexRow">
        <h2>Commencer</h2>
      </div>
      <div className="flexRow justifyAround marginVertical">
        <div className="clickDiv flexColumn padding">
          <p className="marginBottomText">Importer une enquête</p>
          <FontAwesomeIcon icon={faDownload} size="2xl" className="iconFA" />
        </div>
        <div className="clickDiv flexColumn padding" onClick={() => props.onClick()}>
          <p className="marginBottomText">Nouvelle enquête</p>
          <FontAwesomeIcon icon={faPlus} size="2xl" className="iconFA" />
        </div>
      </div>
    </div>
  );
}

export default FormView;
