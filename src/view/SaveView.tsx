import { useContext } from 'react';
import DataContext from './DataContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/SaveView.scss';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import LanguageContext from '../lang/LanguageContext';
import { translate } from '../lang/translate';
import dlIcon from '../assets/dl_icon.png';

function SaveView() {
  const { language } = useContext(LanguageContext);
  const statements = useContext(DataContext);

  console.log(statements);
  const saveViewTranslate = language == 'fr-FR' ? translate.saveView.fr : translate.saveView.en;

  function GenerateJSON() {
    const data = [{}];
    statements.map((element) => {
      if (element.description != undefined) {
        if (element.audio) {
          if (element.image) {
            data.push({
              id: element.id,
              type: 'audio',
              value: element.audio,
              illustration: element.image
            });
          } else {
            data.push({
              id: element.id,
              type: 'audio',
              value: element.audio,
              description: element.description
            });
          }
        } else if (element.image) {
          data.push({
            id: element.id,
            type: 'img',
            value: element.image,
            description: element.description
          });
        } else {
          data.push({ id: element.id, type: 'text', value: element.description });
        }
      }
    });
    const jsonData = JSON.stringify(data, null, 2);
    const blob = new Blob([jsonData], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.download = 'data.json';
    link.click();
  }

  const SaveStudy = () => {
    console.log('not implemented');
  };

  return (
    <Row xs={1} md={2} className="g-4">
      <Col key={1}>
        <Card onClick={SaveStudy}>
          <Card.Body>
            <Card.Title>{saveViewTranslate.SaveButton}</Card.Title>
            <Card.Text>{saveViewTranslate.SaveText}</Card.Text>
          </Card.Body>
          <Card.Img className="image" variant="bottom" src={dlIcon} />
        </Card>
      </Col>
      <Col key={2}>
        <Card onClick={GenerateJSON}>
          <Card.Body>
            <Card.Title>{saveViewTranslate.GenerateButton}</Card.Title>
            <Card.Text>{saveViewTranslate.GenerateText}</Card.Text>
          </Card.Body>
          <Card.Img className="image" variant="bottom" src={dlIcon} />
        </Card>
      </Col>
    </Row>
  );
}

export default SaveView;
