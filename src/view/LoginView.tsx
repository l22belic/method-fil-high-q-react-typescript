import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/LoginView.scss';
import FormView from '../view/FormView';
import homepage from '../assets/homepage.svg';
type Props = {
  onClick: () => void;
};
function LoginView(props: Props) {
  return (
    <div className="gridContainer flexRow">
      <div className="gridItem1 flexRow alignCenter justifyCenter">
        <img src={homepage} width={'90%'} />
      </div>
      <div className="gridItem2 paddingHorizontalLg">
        <div className="gridInfo padding">
          <div className="gridText flexRow alignEnd">
            <h1>Méthode Q</h1>
            <h5 className="marginTextLg">by FILHIGH Q</h5>
          </div>
          <div className="marginVertical">
            <p>
              Construisez et déployer votre questionnaire méthode Q en quelques minutes. Obtenez des
              résultats et des rapports complets pour approfondir votre analyse.
            </p>
          </div>
          <div className="gridLow paddingMd">
            <FormView onClick={props.onClick}></FormView>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginView;
