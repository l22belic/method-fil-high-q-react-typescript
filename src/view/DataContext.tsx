import React from 'react';

export type Statement = {
  id: number;
  description?: string;
  image?: string;
  audio?: string;
};

const DataContext = React.createContext<Statement[]>([]);

export default DataContext;
