import React, { useContext, useState } from 'react';
import '../style/SortManagementView.scss';
import '../style/RulesView.scss';
import '../style/global/tabStructure.scss';
import ButtonGroupComponent from '../component/ButtonGroupComponent';
import SimpleQuestion from '../component/Consigne/Consigne';
import ButtonComponent from '../component/ButtonComponent';
import LanguageContext from '../lang/LanguageContext';
import { translate } from '../lang/translate';

function RulesView() {
  const { language } = useContext(LanguageContext);
  const rulesViewTranslate = language == 'fr-FR' ? translate.rulesView.fr : translate.rulesView.en;

  const [sortGroupSelected, setSortGroupSelected] = useState<boolean[]>([true, false, false]);

  return (
    <div className="container_sort_management_view tabContainer">
      <div className="edit">
        {/* Select sort */}
        <div className="leftBar">
          <ButtonGroupComponent
            buttonGroup={'sort'}
            buttonText={'Général'}
            selected={sortGroupSelected[0]}
            onClick={() => {
              setSortGroupSelected([true, false, false]);
            }}
          ></ButtonGroupComponent>
          <ButtonGroupComponent
            buttonGroup={'sort'}
            buttonText={'Statement'}
            selected={sortGroupSelected[1]}
            onClick={() => {
              setSortGroupSelected([false, true, false]);
            }}
          ></ButtonGroupComponent>
          <ButtonGroupComponent
            buttonGroup={'sort'}
            buttonText={'Entretien'}
            selected={sortGroupSelected[2]}
            onClick={() => {
              setSortGroupSelected([false, false, true]);
            }}
          ></ButtonGroupComponent>
        </div>
      </div>
      <div className="content">
        {sortGroupSelected[0] ? (
          <div className="fieldArea">
            <div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.introGenInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={rulesViewTranslate.zoomInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.startInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={rulesViewTranslate.nextInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.mailInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={rulesViewTranslate.sansSaveInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.finTestInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
            </div>
            <div className="endSelf">
              <ButtonComponent
                buttonText={rulesViewTranslate.btnInput}
                onClick={function (): void {
                  setSortGroupSelected([false, true, false]);
                }}
              ></ButtonComponent>
            </div>
          </div>
        ) : sortGroupSelected[1] ? (
          <div className="fieldArea">
            <div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.statementInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={rulesViewTranslate.tasInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.QSortInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
            </div>
            <div className="endSelf">
              <ButtonComponent
                buttonText={rulesViewTranslate.btnInput}
                onClick={function (): void {
                  setSortGroupSelected([false, false, true]);
                }}
              ></ButtonComponent>
            </div>
          </div>
        ) : (
          <div className="fieldArea">
            <div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={'Introduction'}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={rulesViewTranslate.endText}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.agreeInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={rulesViewTranslate.disagreeInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.dynamiqueInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
                <SimpleQuestion
                  name={'Indication'}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
              <div className="flexInline">
                {' '}
                <SimpleQuestion
                  name={rulesViewTranslate.warningInput}
                  placeholder={rulesViewTranslate.placeholderInput}
                ></SimpleQuestion>
              </div>
            </div>
            <div className="endSelf">
              <ButtonComponent
                buttonText={rulesViewTranslate.btnInput}
                onClick={function (): void {
                  setSortGroupSelected([false, false, true]);
                }}
              ></ButtonComponent>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default RulesView;
