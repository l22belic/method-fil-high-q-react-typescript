import React, { useContext, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/SurveyView.scss';
import '../style/global/tabStructure.scss';
import { Form, InputGroup } from 'react-bootstrap';
import LanguageContext from '../lang/LanguageContext';
import { translate } from '../lang/translate';
import ButtonComponent from '../component/ButtonComponent';
import SurveyCard from '../component/SurveyCard';

type QuestionObject = {
  id: number;
  type: string;
  question: string;
  required: boolean;
  content?: string[];
  from?: string;
  to?: string;
};

type QuestionOption = {
  index: number;
  value: string;
};

let counter = 1;

function SurveyView() {
  const { language } = useContext(LanguageContext);
  const surveyViewTranslate =
    language == 'fr-FR' ? translate.surveyView.fr : translate.surveyView.en;

  const [type, setType] = useState('open');
  const [question, setQuestion] = useState('');
  const [required, setRequired] = useState(false);
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [multipleQData, setMultipleQData] = useState(['']);
  const [questions, setQuestions] = useState<QuestionObject[]>([]);

  const handleChange_inputMultiple = ({ index, value }: QuestionOption) => {
    const newData = [...multipleQData];
    newData[index] = value;
    setMultipleQData(newData);
  };

  const addInputMultiple = () => {
    setMultipleQData((prevData) => [...prevData, '']);
  };

  const removeInputMultiple = ({ index }: { index: number }) => {
    if (multipleQData.length > 2) {
      const newData = [...multipleQData];
      newData.splice(index, 1);
      setMultipleQData(newData);
    }
  };

  return (
    <div className="tabContainer">
      <div className="edit">
        <Form.Select
          aria-label="question type"
          value={type}
          onChange={(v) => {
            setType(v.target.value);
            setMultipleQData(['', '']);
          }}
        >
          <option value="open">{surveyViewTranslate.questionType_open}</option>
          <option value="exMultiple">{surveyViewTranslate.questionType_exMultiple}</option>
          <option value="inMultiple">{surveyViewTranslate.questionType_inMultiple}</option>
          <option value="cursor">{surveyViewTranslate.questionType_cursor}</option>
        </Form.Select>
        <div>
          <InputGroup>
            <Form.Control
              as="textarea"
              placeholder={surveyViewTranslate.questionPlaceHolder}
              onChange={(v) => setQuestion(v.target.value)}
            />
          </InputGroup>
          {type == 'exMultiple' && (
            <div>
              {multipleQData.map((data, index) => (
                <InputGroup key={index}>
                  <InputGroup.Text>○</InputGroup.Text>
                  <Form.Control
                    value={data}
                    onChange={(e) =>
                      handleChange_inputMultiple({ index: index, value: e.target.value })
                    }
                  />
                  {multipleQData.length >= 3 && (
                    <ButtonComponent
                      onClick={() => removeInputMultiple({ index: index })}
                      buttonText={'-'}
                    ></ButtonComponent>
                  )}
                </InputGroup>
              ))}
              <ButtonComponent
                className="addItemButton"
                onClick={addInputMultiple}
                buttonText={'+'}
              ></ButtonComponent>
            </div>
          )}
          {type == 'inMultiple' && (
            <div>
              {multipleQData.map((data, index) => (
                <InputGroup key={index}>
                  <InputGroup.Text>■</InputGroup.Text>
                  <Form.Control
                    value={data}
                    onChange={(e) =>
                      handleChange_inputMultiple({ index: index, value: e.target.value })
                    }
                  />
                  {multipleQData.length >= 3 && (
                    <ButtonComponent
                      onClick={() => removeInputMultiple({ index: index })}
                      buttonText={'-'}
                    ></ButtonComponent>
                  )}
                </InputGroup>
              ))}
              <ButtonComponent
                className="addItemButton"
                onClick={addInputMultiple}
                buttonText={'+'}
              ></ButtonComponent>
            </div>
          )}
          {type == 'cursor' && (
            <div>
              <InputGroup>
                <Form.Control
                  type="text"
                  placeholder={surveyViewTranslate.questionType_cursorFrom}
                  onChange={(v) => v.target.value != '' && setFrom(v.target.value)}
                />
              </InputGroup>
              <InputGroup>
                <Form.Control
                  type="text"
                  placeholder={surveyViewTranslate.questionType_cursorTo}
                  onChange={(v) => v.target.value != '' && setTo(v.target.value)}
                />
              </InputGroup>
            </div>
          )}
          <InputGroup>
            <Form.Check
              id="openCheck"
              label={surveyViewTranslate.questionRequired}
              onChange={() => setRequired(!required)}
            />
          </InputGroup>
          <div id="errorContainer"></div>
        </div>

        <ButtonComponent
          className="addButton"
          buttonText={surveyViewTranslate.createButtonLabel}
          onClick={() => {
            const content = multipleQData.filter((s) => s != '');
            switch (type) {
              case 'open':
                setQuestions([
                  ...questions,
                  {
                    id: counter,
                    type,
                    question,
                    required
                  }
                ]);
                break;
              case 'exMultiple':
                setQuestions([
                  ...questions,
                  {
                    id: counter,
                    type,
                    question,
                    required,
                    content
                  }
                ]);
                break;
              case 'inMultiple':
                setQuestions([
                  ...questions,
                  {
                    id: counter,
                    type,
                    question,
                    required,
                    content
                  }
                ]);
                break;
              case 'cursor':
                setQuestions([
                  ...questions,
                  {
                    id: counter,
                    type,
                    question,
                    required,
                    from,
                    to
                  }
                ]);
                break;
              default:
                console.log('unknown type');
                break;
            }
            counter++;
          }}
        ></ButtonComponent>
      </div>
      <div className="content">
        {questions.map((question) => {
          return (
            <li key={question.id}>
              <SurveyCard
                id={question.id}
                type={question.type}
                question={question.question}
                required={question.required}
                from={question.from}
                to={question.to}
                content={question.content}
              />
            </li>
          );
        })}
      </div>
    </div>
  );
}

export default SurveyView;
