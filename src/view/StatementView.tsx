import React, { useContext, useState } from 'react';
import DataContext, { Statement } from './DataContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/StatementView.scss';
import { translate } from '../lang/translate';
import LanguageContext from '../lang/LanguageContext';
import DocumentUploader from '../component/DocumentUploader';
import ButtonComponent from '../component/ButtonComponent';
import StatementCard from '../component/StatementCard';

// Number of statements
let counter = 1;
function StatementView() {
  const { language } = useContext(LanguageContext);
  const statementViewTranslate =
    language == 'fr-FR' ? translate.statementView.fr : translate.statementView.en;

  // Data of future statement
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');
  const [audio, setAudio] = useState('');
  const statements = useContext<Statement[]>(DataContext);
  const [statementsList, setStatementsList] = useState(statements);
  return (
    <div className="statementView">
      <div className="edit">
        <h2>{statementViewTranslate.creationTitle}</h2>
        <div className="descriptionSection">
          <h3>{statementViewTranslate.descriptionLabel}</h3>
          <textarea
            placeholder={statementViewTranslate.textAreaPlaceholder}
            className="textArea"
            value={description}
            onChange={(v) => setDescription(v.target.value)}
          ></textarea>
        </div>
        {description == 'Capucine' && <div>{description}</div>}
        <div className="imageSection">
          <h3>{statementViewTranslate.imageLabel}</h3>
          <DocumentUploader
            id={1}
            isImage
            onRemoveDocument={() => setImage('')}
            onDocumentUpload={function (url: string): void {
              setImage(url);
            }}
          />
        </div>
        <div className="audioSection">
          <h3>{statementViewTranslate.audioLabel}</h3>
          <DocumentUploader
            id={2}
            isAudio
            onRemoveDocument={() => setAudio('')}
            onDocumentUpload={function (url: string): void {
              setAudio(url);
            }}
          />
        </div>
        <div className="actionButtons">
          <ButtonComponent
            disabled={image == '' && audio == '' && description == ''}
            className="actionButton"
            buttonText={statementViewTranslate.createButtonLabel}
            onClick={() => {
              // Add new statement
              const st: Statement = {
                id: counter,
                description: description,
                image: image,
                audio: audio
              };
              statements.push(st);
              setStatementsList([...statements]);
              // Increase statement number
              counter++;

              // Reset current statement
              setDescription('');
              setImage('');
              setAudio('');
            }}
          />
          <div>
            <ButtonComponent
              className="actionButton"
              buttonText={statementViewTranslate.importFileButtonLabel}
              isSecondary={true}
              onClick={function (): void {
                throw new Error('Function not implemented.');
              }}
            />
            <ButtonComponent
              className="actionButton"
              buttonText={statementViewTranslate.downloadStaButtonLabel}
              isSecondary={true}
              onClick={function (): void {
                throw new Error('Function not implemented.');
              }}
            />
          </div>
        </div>
      </div>

      <div className="content">
        <h2>{statementViewTranslate.statementTitle}</h2>
        <ul>
          {statementsList.map((statement) => {
            return (
              <li className="cardListElement" key={'statement-' + statement.id}>
                <StatementCard
                  id={statement.id}
                  cardAudio={statement.audio}
                  cardImage={statement.image}
                  cardText={statement.description}
                  onDelete={() => {
                    const idToDelete = statement.id;
                    const newTab = statements.filter((st) => st.id !== statement.id);
                    const tab = newTab.map((st) => {
                      if (st.id < idToDelete) {
                        return st;
                      }
                      return { ...st, id: st.id - 1 };
                    });
                    statements.splice(0, statements.length);
                    tab.forEach((element) => {
                      statements.push(element);
                    });
                    setStatementsList([...statements]);
                    counter--;
                  }}
                />
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}

export default StatementView;
