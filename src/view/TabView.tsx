import { useContext, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Tab, Tabs } from 'react-bootstrap';
import StatementView from './StatementView';
import SortManagementView from './SortManagementView';
import SurveyView from './SurveyView';
import SaveView from './SaveView';
import RulesView from './RulesView';
import '../style/TabView.scss';
import LanguageContext from '../lang/LanguageContext';
import { translate } from '../lang/translate';
import DataContext, { Statement } from './DataContext';

function TabView() {
  const { language } = useContext(LanguageContext);
  const [statements] = useState<Statement[]>([]);

  const tabViewTranslate = language == 'fr-FR' ? translate.tabView.fr : translate.tabView.en;

  return (
    <Tabs
      defaultActiveKey="statement"
      transition={false}
      id="noanim-tab-example"
      className="mb-3"
      justify
    >
      <Tab
        eventKey="statement"
        title={
          <div>
            <div className="imgSelected"></div>
            <span>{tabViewTranslate.statementTitle}</span>
          </div>
        }
      >
        <DataContext.Provider value={statements}>
          <StatementView />
        </DataContext.Provider>
      </Tab>
      <Tab
        eventKey="sortManagement"
        title={
          <div>
            <div className="imgSelected"></div>
            <span>{tabViewTranslate.sortTitle}</span>
          </div>
        }
      >
        <SortManagementView />
      </Tab>
      <Tab
        eventKey="survey"
        title={
          <div>
            <div className="imgSelected"></div>
            <span>{tabViewTranslate.surveyTitle}</span>
          </div>
        }
      >
        <SurveyView />
      </Tab>
      <Tab
        eventKey="rules"
        title={
          <div>
            <div className="imgSelected"></div>
            <span>{tabViewTranslate.rulesTitle}</span>
          </div>
        }
      >
        <RulesView />
      </Tab>
      <Tab
        eventKey="save"
        title={
          <div>
            <div className="imgSelected"></div>
            <span>{tabViewTranslate.saveTitle}</span>
          </div>
        }
      >
        <DataContext.Provider value={statements}>
          <SaveView />
        </DataContext.Provider>
      </Tab>
    </Tabs>
  );
}

export default TabView;
